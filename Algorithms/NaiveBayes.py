
import random

#Chia bộ test và traning
def splitDataset(dataset, splitRatio):#splitRatio tỉ lệ traning
    trainSize = int(len(dataset) * splitRatio)# size tập traning
    trainSet = [] # chứa tập traning
    copy = list(dataset)# tập test
    while len(trainSet) < trainSize:# chia hai tập
        index = random.randrange(len(copy))
        trainSet.append(copy.pop(index))
    return [trainSet, copy]


#kiểm độ chính sác
def getAccuracy(testSet, predictions):
    correct = 0
    for i in range(len(testSet)):# đi từng phần tử
        if testSet[i][-1] == predictions[i]:#kiểm tra dự đoán và lết quả thực
            correct += 1
    return (correct / float(len(testSet))) * 100.0

#Phân chia lại khoảng của dữ liệu số, Phân bổ theo nhóm. Nhóm có độ cao và thấp()
def Fix(trainingSet):
    Max =[]# giá trị ao nhất
    Min=[]# giá trị thấp nhất
    position = [0,1,2,3,4,5,6,7,8,9,11,12]# các thuộc tính là số
    for i in position: #Tìm min max của từng thuộc tính
        max = 0.0
        min = 0.0
        for j in trainingSet:#Lần lượt so sánh các phần tử trong 1 cột
            if(float(j[i])>max):
                max = float(j[i])
            if(float(j[i])<min):
                min = float(j[i])
        Max.append(max)#app giá trị lớn nhất của thuộc tính vô mảng
        Min.append(min)
    for i in position:# điều chế lại dữ liệu
        for j in trainingSet:# đi từng phân tự trong tập traning
            b = True#Điều khiển vòng lặp
            c = 1 # 1 là khoảng min -> giữa 2 là giữa-> max
            k= 0.0 # mức độ chia lớp dữ liệu , ở đây mình chia 0.5 thì sẽ thành 2 lớp , dùng 0.5 vì sau nhiều lần thử mình thấy số này là ok
            while (b):#bài toán tìm xem giá trị thuộc khoảng chia nào
                if ((float(j[i])>=(min + k*(max-min))) and (float(j[i])<(min+(k+0.5)*(max-min)))):
                    j[i] = c# đúng khoản thì gán
                    b = False# kết thúc vòng lặp
                c = c+1
                k= k + 0.5
    return trainingSet # trả về tập dữ liệu đã đc chế biến
def Traning(trainingSet):#traning
    data = Fix(trainingSet)# Xử lý dữ liệu
    dist = { 1:{}, 2:{},3:{},4:{},5:{},6:{},7:{},8:{},9:{},10:{},11:{},12:{},13:{},14:{},15:{},'True':0.0,'False':0.0}# chứa mô hình phân lớp navi bayes
    k = 1# chạy các phần tử trong dist
    for i in trainingSet:# bắt đầy traning
        if(i[-1]=='True'):# đếm số thuộc tính quyết định true = ? false =?
            dist['True']=dist['True']+1.0
        else:
            dist['False']=dist['False']+1.0
    for i in range(15):# chạy từng cột
        for j in data:#chạy từng hàng
            if(list(dist[k].keys()).count(str(j[i]))==0):#kiểm tra giá trị đã có trong dist chưa , chưa có thì thêm
                dist[k][str(j[i])] = {'False': 0.0 ,'True':0.0}# false và true số lượng kết quả ứng với giá trị này
                if(j[15]=='False'):#kiểm tra thuộc tính quyết định là j
                    dist[k][str(j[i])]['False']=1.0#là false thì tăng false của giá trị
                else:
                    dist[k][str(j[i])]['True'] = 1.0
            else:
                if (j[15] == 'False'):#Nếu đã có trong dist thì kiểm tra thuộc tính quyết định và cộng thôi
                    dist[k][str(j[i])]['False'] = dist[k][str(j[i])]['False']+1.0
                else:
                   dist[k][str(j[i])]['True'] = dist[k][str(j[i])]['True'] + 1.0
        k=k+1
    return  dist
def test(data,dist):# test phân lớp baze
    result = [] # lưu kết quả dự đoán
    testdata= Fix(data) # chế biến dữu liệu test
    for i in testdata:# dự đoán từng hàng
        true = 1.0#xác xuất mua khởi tạo bằng 1 để nhân
        false= 1.0#xác xuất ko khởi tạo bằng 1 để nhân
        k=1
        while(k<15):# chạy các phần tử trong dist
            t=0#ko chạy đến giá trị quyết định
            for j in i:# đi từng giá trị của hàng đang xét
                if(t!=15):#nếu vẫm chưa đến cột cuối cùng
                    j = str(j)#ép kểu để lấy khóa key
                    if(list(dist[k].keys()).count(j)!=0):#nếu giá trịnh đang xét ko có trong dist, bỏ qua
                        true = true *(float(dist[k][j]['True']))/(float(dist['True']))
                        false = false * (float(dist[k][j]['False'])) / (float(dist['False']))
                    t=t+1
            true = true * (float(dist['True'])/(float((dist['True'])+float(dist['False'])))) # tính xác xuất
            false = false * (float(dist['False'])/(float((dist['True'])+float(dist['False']))))
            if(true ==0.0):#trường hợp nhân bằng 0 cho để ko gặp lỗi , số rất nhỏ nên ko ảnh hưởng đến
                true=0.000000000000000000000000000000000000000000000001
            if(false==0):
                false=0.00000000000000000000000000000000000000000000000001
            t = true / (true + false)#tính dự đoán
            f = false / (true + false)
            if(t>f):
                result.append('True') # đưa  ra dự đoán
            else:
                result.append('False')
            k=k+1
    return  result# trả về mảng kết quả dự đoán

def naivebayes(dataset):# hàm chính
    splitRatio = 0.67#tỉ lệ chia tập
    trainingSet, testSet = splitDataset(dataset, splitRatio) # chia tập traning và test
    # prepare model

    dist = Traning(trainingSet)#Tạo mô hình phân lớp
    result = test(testSet,dist)#test mô hình
    return dist #trả về mô hình
