import pandas as pd
import TienXuLy
import Algorithms as al


df=pd.read_csv("DataSet/DataSet_Version_01.csv")
dict_data=df.to_dict()

# -------------- 1. CHỌN LỌC DỮ LIỆU ----------------------
# LOẠI BỎ THUỘC TÍNH "OPERATING SYSTEM" VÀ "BROWSER";
dict_data = TienXuLy.ChonLocDuLieu(dict_data, ["OperatingSystems", "Browser"])

# -------------- 2. XỬ LÝ SAI & THIẾU DỮ LIỆU --------------
dict_data= TienXuLy.XuLyDuLieuSaiThieu(dict_data)

# ---------------3. XỬ LÝ TRÙNG LẶP -----------------------
# LOẠI BỎ CÁC BỘ DỮ LIỆU TRÙNG LẶP VỚI NHAU
dict_data = TienXuLy.XuLyTrunglap(dict_data)

# ---------------4. XỬ LÝ MÂU THUẪN -----------------------
# LOẠI BỎ CÁC BỘ DỮ LIỆU MÂU THUẪN VỚI NHAU
df = pd.DataFrame.from_dict(dict_data)
dict_data_index = df.to_dict(orient="index");  # Đọc dữ liệu dưới định hướng index. --> Dữ liệu theo dòng.
dict_data = TienXuLy.XuLyMauThuan(dict_data_index)

# ---------------5. XỬ LÝ DỮ LIỆU NHIỄU -------------------
dict_data = TienXuLy.XuLyDuLieuNhieu(dict_data)

TienXuLy.PhanTichDuLieu(dict_data)