import pandas as pd
import TienXuLy
import Algorithms as al
import numpy as np
import csv

# -------------------------------------------------ĐỌC DATASET-------------------------------------------------------

df = pd.read_csv("DataSet/DataSet_Version_01.csv")
dict_data = df.to_dict()

# =====================================HÀM ĐIỀU KHIỂN CHƯƠNG TRÌNH===================================================
query = "-1"  # Biến lưu yêu cầu của người dùng
isContinue = True  # Biến điều khiển trạng thái của chương trình True : tiếp tục.  False : dừng chương trình
# Khai báo biến kiểu từ điển lưu trạng thái từng khâu xử lý => Kiểm soát các giai đoạn
preProcessState = {"Phân tích cơ sở dữ liệu": 0,
                   "Chọn lọc dữ liệu": 0,
                   "Xử lý sai và thiếu dữ liệu": 0,
                   "Xử lý trùng lặp": 0,
                   "Xử lý mâu thuẫn dữ liệu": 0,
                   "Xử lý dữ liệu nhiễu": 0,
                   "Phân chia tập dữ liệu": 0,
                   "Xây dựng mô hình theo giải thuật ID3": 0,
                   "Xây dựng mô hình theo giải thuật NaiveBayes": 0}

while isContinue == True :
    print("\n---------------------------------------------------------------------")
    print("------CHƯƠNG TRÌNH PHÂN TÍCH HÀNH VI KHÁCH HÀNG MUA HÀNG ONLINE------")
    print("All: Chạy tất cả các xử lý theo cài đặt mặc định")
    print("Data: Xuất tập cơ sở dữ liệu hiện tại của hệ thống")
    print("---------------------------------------------------------------------")
    print("*GIAI ĐOẠN 1 - TIỀN XỬ LÝ DỮ LIỆU")
    print("1: Chạy tiền xử lý toàn diện")
    print("1.1: Phân tích cơ sở dữ liệu")
    print("1.2: Chọn lọc dữ liệu")
    print("1.3: Xử lý sai và thiếu dữ liệu")
    print("1.4: Xử lý trùng lặp")
    print("1.5: Xử lý mâu thuẫn dữ liệu")
    print("1.6: Xử lý dữ liệu nhiễu")
    print("1.7: Phân chia tập dữ liệu")
    print("*GIAI ĐOẠN 2 - HUẤN LUYỆN, XÂY DỰNG MÔ HÌNH")
    print("2: Xây dựng mô hình theo ID3 và Naive Bayes")
    print("2.1: Mô hình cây quyết tính - Giải thuật ID3")
    print("2.2: Mô hình xác suất - Giải thuật Naive Bayes")
    print("*GIAI ĐOẠN 3 - ĐÁNH GIÁ MÔ HÌNH")
    print("3: Chạy đánh giá toàn diện 2 mô hình")
    print("3.1: Đánh giá toàn diện mô hình xây dựng theo giải thuật ID3")
    print("3.2: Đánh giá toàn diện mô hình xây dựng theo giải thuật NaiveBayes")
    print("---------------------------------------------------------------------")
    print("Exit : Dừng chương trình")
    print("LƯU Ý : CÁC TÍNH NĂNG TRONG CÁC GIAI ĐOẠN CHỈ THỰC THI CHÍNH XÁC KHI TOÀN BỘ TÍNH NĂNG Ở GIAI ĐOẠN TRƯỚC ĐÓ ĐÃ ĐƯỢC THỰC THI")
    query = input("=>Enter your query: ");
    if query == "All":
        # ======================================GIAI ĐOẠN 1 : TIỀN XỬ LÝ DỮ LIỆU===============================================

        # --------------------------------------1.0. PHÂN TÍCH CƠ SỞ DỮ LIỆU --------------------------------------------------

        TienXuLy.PhanTichDuLieu(dict_data)

        # --------------------------------------1.1. CHỌN LỌC DỮ LIỆU----------------------------------------------------------

        # LOẠI BỎ THUỘC TÍNH "OPERATING SYSTEM" VÀ "BROWSER";
        dict_data = TienXuLy.ChonLocDuLieu(dict_data, ["OperatingSystems", "Browser"])

        # --------------------------------------1.2. XỬ LÝ SAI & THIẾU DỮ LIỆU--------------------------------------------------

        dict_data = TienXuLy.XuLyDuLieuSaiThieu(dict_data)

        # --------------------------------------1.3. XỬ LÝ TRÙNG LẶP------------------------------------------------------------
        # LOẠI BỎ CÁC BỘ DỮ LIỆU TRÙNG LẶP VỚI NHAU
        dict_data = TienXuLy.XuLyTrunglap(dict_data)

        # --------------------------------------1.4. XỬ LÝ MÂU THUẪN------------------------------------------------------------

        # LOẠI BỎ CÁC BỘ DỮ LIỆU MÂU THUẪN VỚI NHAU
        df = pd.DataFrame.from_dict(dict_data)
        dict_data_index = df.to_dict(orient="index");  # Đọc dữ liệu dưới định hướng index. --> Dữ liệu theo dòng.
        dict_data = TienXuLy.XuLyMauThuan(dict_data_index)

        # --------------------------------------1.5. XỬ LÝ DỮ LIỆU NHIỄU--------------------------------------------------------

        dict_data = TienXuLy.XuLyDuLieuNhieu(dict_data)

        # --------------------------------------1.6. PHÂN CHIA TẬP DỮ LIỆU-------------------------------------------------------

        arr = TienXuLy.ChiaTapDuLieu(dict_data, 0.3, 0.7)
        # Hàm trả về mảng các tập theo thứ tự sau array[data_train,data_test,target_train,target_test]
        data_train = arr[0]
        data_test = arr[1]
        full_data = np.append(data_train, data_test, axis=0)
        target_train = arr[2]
        target_test = arr[3]
        full_target = np.append(target_train, target_test, axis=0)
        list_attr = list(dict_data.keys())
        del list_attr[-1]
        attributes = list_attr  # Danh sách các thuộc tính điều kiện

        # =======================================GIAO ĐOAN 2 : HUẤN LUYỆN TẠO MÔ HÌNH==============================================

        # ---------------------------------------2.1.ID3 DECISION TREE ALGORITHM----------------------------------------------

        # Tạo dữ liệu đầu vào cho huấn luyện và kiểm thử
        id3 = al.DecisionTree()
        df_data_train = pd.DataFrame.from_dict(data_train)
        df_target_train = pd.DataFrame.from_dict(target_train)
        dataTrain = id3.data_target_merge(df_data_train, df_target_train, attributes, "Revenue")  # Có sẵn nhãn lớp
        id3.fit(dataTrain, attributes)

        # ----------------------------------------2.2 NAIVE BAYES ALGORITHM----------------------------------------------------

        df2 = pd.DataFrame.from_dict(dict_data)  # chuyển về dataframe
        filename = "datasetBZ.csv"
        df2.to_csv(filename, index=False)  # xuất file csv
        # df2.to_excel("Result.xlsx")
        lines = csv.reader(open(filename))#đọc file
        dataset = list(lines)#chuyển về list
        del dataset[0]#Xóa nhãn
        dictnv= al.naivebayes(dataset)#Xây dựng Navi Bayes
        print("### XÂY DỰNG MÔ HÌNH THEO GIẢI THUẬT NAIVE BAYES THÀNH CÔNG");

        # ==========================================GIAI ĐOẠN 3 :  KIỂM THỬ MÔ HÌNH============================================

        # -----------------------------------------3.1 ĐÁNH GIÁ MÔ HÌNH ID3--------------------------------------------------
        # NẾU MUỐN KIỂM THỬ BẰNG BỘ DỮ LIỆU NGẪU NHIÊN NHẬP VÀO --> CHUẨN HOÁ BỘ DỮ LIỆU ĐẦU VÀO.
        # CHUẨN HOÁ DỮ LIỆU CẦN PHÂN LỚP
        # data_request=[0,0,0,0,1,0,0.2,0.2,0,0,"Feb",1,1,1,1,"Returning_Visitor",False]
        # data_request=TienXuLy.ChuanHoaDuLieu(dict_data,data_request)
        df_data_test = pd.DataFrame.from_dict(data_test)
        df_target_test = pd.DataFrame.from_dict(target_test)
        dataTest = id3.data_test(df_data_test, attributes)  # Không có nhãn lớp
        dataTrain_No_Label = id3.data_test(df_data_train, attributes)
        pred = id3.predict(dataTest)
        pred_on_datatrain = id3.predict(dataTrain_No_Label)

        print("------------ĐÁNH GIÁ MÔ HÌNH ID3 --------------------")
        utils = al.utils()
        KFold = al.KFold()
        # Vẽ ma trận nhầm lẫn và tính toán các độ đo
        print("Các Độ đo trên tập dữ liệu test")
        utils.print_metrics(target_test, pred)
        print("Các Độ đo trên tập dữ liệu train")
        utils.print_metrics(target_train, pred_on_datatrain)

        # Tính độ đo KFold
        df_full_data = pd.DataFrame.from_dict(full_data)
        df_full_target = pd.DataFrame.from_dict(full_target)
        full_data_target = id3.data_target_merge(df_full_data, df_full_target, attributes,
                                                 "Revenue")  # Tạo tập dữ liệu test có nhãn lớp
        scores = KFold.predict(full_data_target, id3, n_splits=20)
        utils.print_scores(scores)

        # ------------------------------------------3.2. ĐÁNH GIÁ MÔ HÌNH NAIVE BAYES-----------------------------------------------------
        # #Kiem thu 20 lan chế lại của KFold
        print('----------ĐÁNH GIÁ MÔ HÌNH NAIVE BAYES -------')
        print('----------KỸ THUẬT KFOLD 20 LẦN')
        so = []
        for i in range(20):# chạy 20 lần
            d , t = al.splitDataset(dataset,0.7)# tạo tập test lấy 30% bất kỳ
            so.append(al.getAccuracy(t,al.test(t,dictnv)))# chạy navi bayes
        utils = al.utils()
        utils.print_scores(so)# vẽ hình
    elif query == "Data":
        print("Dữ liệu của tập DATASET")
        print(dict_data)
    elif query == "1":
        # Phân tích dữ liệu
        TienXuLy.PhanTichDuLieu(dict_data)
        # Chọn lọc dữ liệu
        dict_data = TienXuLy.ChonLocDuLieu(dict_data, ["OperatingSystems", "Browser"])
        # Xử lý dữ liệu sai thiếu
        dict_data = TienXuLy.XuLyDuLieuSaiThieu(dict_data)
        # Xử lý dữ liệu trùng lặp
        dict_data = TienXuLy.XuLyTrunglap(dict_data)
        # Xử lý dữ liệu mau thuẫn
        df = pd.DataFrame.from_dict(dict_data)
        dict_data_index = df.to_dict(orient="index");  # Đọc dữ liệu dưới định hướng index. --> Dữ liệu theo dòng.
        dict_data = TienXuLy.XuLyMauThuan(dict_data_index)
        # Xử lý dữ liệu nhiễu
        dict_data = TienXuLy.XuLyDuLieuNhieu(dict_data)
        # Chia tập dữ liệu
        arr = TienXuLy.ChiaTapDuLieu(dict_data, 0.3, 0.7)
        # Hàm trả về mảng các tập theo thứ tự sau array[data_train,data_test,target_train,target_test]
        data_train = arr[0]
        data_test = arr[1]
        target_train = arr[2]
        target_test = arr[3]
        full_data = np.append(data_train, data_test, axis=0)
        full_target = np.append(target_train, target_test, axis=0)
        list_attr = list(dict_data.keys())
        # Xoá thuộc tính quyết định
        del list_attr[-1]
        attributes = list_attr  # Danh sách các thuộc tính điều kiện
        # Cập nhật trạng thái của 7 thao tác tiền xử lý
        for i in preProcessState:
            if (i == "Xây dựng mô hình theo giải thuật ID3"):
                break;
            else:
                preProcessState[i] = 1
    elif query == "1.1":
        # Phân tích dữ liệu
        TienXuLy.PhanTichDuLieu(dict_data)
        preProcessState["Phân tích cơ sở dữ liệu"] = 1
    elif query == "1.2":
        # Chọn lọc dữ liệu
        dict_data = TienXuLy.ChonLocDuLieu(dict_data, ["OperatingSystems", "Browser"])
        preProcessState["Chọn lọc dữ liệu"] = 1
    elif query == "1.3":
        # Xử lý dữ liệu sai thiếu
        dict_data = TienXuLy.XuLyDuLieuSaiThieu(dict_data)
        preProcessState["Xử lý sai và thiếu dữ liệu"] = 1
    elif query == "1.4":
        # Xử lý dữ liệu trùng lặp
        dict_data = TienXuLy.XuLyTrunglap(dict_data)
        preProcessState["Xử lý trùng lặp"] = 1
    elif query == "1.5":
        # Xử lý dữ liệu mâu thuẫn
        df = pd.DataFrame.from_dict(dict_data)
        dict_data_index = df.to_dict(orient="index");  # Đọc dữ liệu dưới định hướng index. --> Dữ liệu theo dòng.
        dict_data = TienXuLy.XuLyMauThuan(dict_data_index)
        preProcessState["Xử lý mâu thuẫn dữ liệu"] = 1
    elif query == "1.6":
        # Xử lý dữ liệu nhiễu
        dict_data = TienXuLy.XuLyDuLieuNhieu(dict_data)
        preProcessState["Xử lý dữ liệu nhiễu"] = 1
    elif query == "1.7":
        train_ratio = int(input("Nhập tỉ lệ cho tập huấn luyện (0<=k<=1) : "))
        # Chia tập dữ liệu
        arr = TienXuLy.ChiaTapDuLieu(dict_data, 1 - train_ratio, train_ratio)
        # Hàm trả về mảng các tập theo thứ tự sau array[data_train,data_test,target_train,target_test]
        data_train = arr[0]
        data_test = arr[1]
        target_train = arr[2]
        target_test = arr[3]
        full_data = np.append(data_train, data_test, axis=0)
        full_target = np.append(target_train, target_test, axis=0)
        list_attr = list(dict_data.keys())
        # Xoá thuộc tính quyết định
        del list_attr[-1]
        attributes = list_attr  # Danh sách các thuộc tính điều kiện
        preProcessState["Phân chia tập dữ liệu"] = 1
    elif query == "2":
        # Chạy xây dựng 2 mô hình
        # Kiểm tra xem GĐ1 đã hoàn thành hay chưa
        isReady = True  # Biến điều khiển có chạy xử lý GĐ2 hay không
        for i in preProcessState:
            if i == "Xây dựng mô hình theo giải thuật ID3":
                break
            if preProcessState[i] != 1:
                print("ERROR : " + i + " ở GĐ1 Chưa được xử lý. Không thể chạy xử lý GĐ2")
                isReady = False
                break;
        if isReady:
            # ---------------------------------------2.1.ID3 DECISION TREE ALGORITHM----------------------------------------------
            # Tạo dữ liệu đầu vào cho huấn luyện và kiểm thử
            id3 = al.DecisionTree()
            df_data_train = pd.DataFrame.from_dict(data_train)
            df_target_train = pd.DataFrame.from_dict(target_train)
            dataTrain = id3.data_target_merge(df_data_train, df_target_train, attributes, "Revenue")  # Có sẵn nhãn lớp
            id3.fit(dataTrain, attributes)
            preProcessState["Xây dựng mô hình theo giải thuật ID3"] = 1
            # ---------------------------------------2.2.Naive Bayes--------------------------------
            df2 = pd.DataFrame.from_dict(dict_data)  # chuyển về dataframe
            filename = "datasetBZ.csv"
            df2.to_csv(filename, index=False)  # xuất file csv
            # df2.to_excel("Result.xlsx")
            lines = csv.reader(open(filename))#đọc file
            dataset = list(lines)#chuyển về list
            del dataset[0]#Xóa nhãn
            dictnv= al.naivebayes(dataset)#Xây dựng Navi Bayes
            print("MÔ HÌNH XÂY DỰNG THÀNH CÔNG")
            preProcessState["Xây dựng mô hình theo giải thuật NaiveBayes"] = 1
    elif query == "2.1":
        # Kiểm tra xem GĐ1 đã hoàn thành hay chưa
        isReady = True  # Biến điều khiển có chạy xử lý GĐ2 hay không
        for i in preProcessState:
            if i == "Xây dựng mô hình theo giải thuật ID3":
                break
            if preProcessState[i] != 1:
                print("ERROR : " + i + " ở GĐ1 Chưa được xử lý. Không thể chạy xử lý GĐ2")
                isReady = False
                break;
        if isReady:
            # ---------------------------------------2.1.ID3 DECISION TREE ALGORITHM----------------------------------------------
            # Tạo dữ liệu đầu vào cho huấn luyện và kiểm thử
            id3 = al.DecisionTree()
            df_data_train = pd.DataFrame.from_dict(data_train)
            df_target_train = pd.DataFrame.from_dict(target_train)
            dataTrain = id3.data_target_merge(df_data_train, df_target_train, attributes, "Revenue")  # Có sẵn nhãn lớp
            id3.fit(dataTrain, attributes)
            preProcessState["Xây dựng mô hình theo giải thuật ID3"] = 1
    elif query == "2.2":
        # Kiểm tra xem GĐ1 đã hoàn thành hay chưa
        isReady = True  # Biến điều khiển có chạy xử lý GĐ2 hay không
        for i in preProcessState:
            if i == "Xây dựng mô hình theo giải thuật ID3":
                break
            if preProcessState[i] != 1:
                print("ERROR : " + i + " ở GĐ1 Chưa được xử lý. Không thể chạy xử lý GĐ2")
                isReady = False
                break;
        if isReady:
            df2 = pd.DataFrame.from_dict(dict_data)  # chuyển về dataframe
            filename = "datasetBZ.csv"
            df2.to_csv(filename, index=False)  # xuất file csv
            # df2.to_excel("Result.xlsx")
            lines = csv.reader(open(filename))#đọc file
            dataset = list(lines)#chuyển về list
            del dataset[0]#Xóa nhãn
            dictnv= al.naivebayes(dataset)#Xây dựng Naive Bayes
            print("MÔ HÌNH XÂY DỰNG THÀNH CÔNG")
            preProcessState["Xây dựng mô hình theo giải thuật NaiveBayes"] = 1
    elif query == "3":
        # Kiểm tra xem GĐ2 đã hoàn thành hay chưa
        isReady = True  # Biến điều khiển có chạy xử lý GĐ2 hay không
        if preProcessState["Xây dựng mô hình theo giải thuật ID3"] != 1:
            print("ERROR : GĐ2 Chưa được xử lý. Không thể chạy xử lý GĐ3")
            isReady = False
        if preProcessState["Xây dựng mô hình theo giải thuật NaiveBayes"] != 1:
            print("ERROR : GĐ2 Chưa được xử lý. Không thể chạy xử lý GĐ3")
            isReady = False
        if isReady:
            # -----------------------------------------3.1 ĐÁNH GIÁ MÔ HÌNH ID3--------------------------------------------------
            # NẾU MUỐN KIỂM THỬ BẰNG BỘ DỮ LIỆU NGẪU NHIÊN NHẬP VÀO --> CHUẨN HOÁ BỘ DỮ LIỆU ĐẦU VÀO.
            # CHUẨN HOÁ DỮ LIỆU CẦN PHÂN LỚP
            # data_request=[0,0,0,0,1,0,0.2,0.2,0,0,"Feb",1,1,1,1,"Returning_Visitor",False]
            # data_request=TienXuLy.ChuanHoaDuLieu(dict_data,data_request)
            df_data_test = pd.DataFrame.from_dict(data_test)
            df_target_test = pd.DataFrame.from_dict(target_test)
            dataTest = id3.data_test(df_data_test, attributes)  # Không có nhãn lớp
            dataTrain_No_Label = id3.data_test(df_data_train, attributes)
            pred = id3.predict(dataTest)
            pred_on_datatrain = id3.predict(dataTrain_No_Label)

            print("------------ĐÁNH GIÁ MÔ HÌNH ID3 --------------------")
            utils = al.utils()
            KFold = al.KFold()
            # Vẽ ma trận nhầm lẫn và tính toán các độ đo
            print("========Các Độ đo trên tập dữ liệu test=========")
            utils.print_metrics(target_test, pred)
            print("========Các Độ đo trên tập dữ liệu train=========")
            utils.print_metrics(target_train, pred_on_datatrain)

            # Tính độ đo KFold
            df_full_data = pd.DataFrame.from_dict(full_data)
            df_full_target = pd.DataFrame.from_dict(full_target)
            full_data_target = id3.data_target_merge(df_full_data, df_full_target, attributes,
                                                     "Revenue")  # Tạo tập dữ liệu test có nhãn lớp
            scores = KFold.predict(full_data_target, id3, n_splits=20)
            utils.print_scores(scores)

            # ------------------------------------------3.2. ĐÁNH GIÁ MÔ HÌNH NAIVE BAYES-----------------------------------------------------
            print('----------ĐÁNH GIÁ MÔ HÌNH NAIVE BAYES -------')
            print('--------KẾT QUẢ ĐÁNH GIÁ KỸ THUẬT KFOLD 20 LẦN ')
            so = []
            for i in range(20):# chạy 20 lần
                d , t = al.splitDataset(dataset,0.7)# tạo tập test lấy 30% bất kỳ
                so.append(al.getAccuracy(t,al.test(t,dictnv)))# chạy navi bayes
            utils = al.utils()
            utils.print_scores(so)# vẽ hình
    elif query == "3.1":
        # Kiểm tra xem GĐ2 đã hoàn thành hay chưa
        isReady = True  # Biến điều khiển có chạy xử lý GĐ2 hay không
        if preProcessState["Xây dựng mô hình theo giải thuật ID3"] != 1:
            isReady = False
        if isReady:
            # -----------------------------------------3.1 ĐÁNH GIÁ MÔ HÌNH ID3--------------------------------------------------
            # NẾU MUỐN KIỂM THỬ BẰNG BỘ DỮ LIỆU NGẪU NHIÊN NHẬP VÀO --> CHUẨN HOÁ BỘ DỮ LIỆU ĐẦU VÀO.
            # CHUẨN HOÁ DỮ LIỆU CẦN PHÂN LỚP
            # data_request=[0,0,0,0,1,0,0.2,0.2,0,0,"Feb",1,1,1,1,"Returning_Visitor",False]
            # data_request=TienXuLy.ChuanHoaDuLieu(dict_data,data_request)
            df_data_test = pd.DataFrame.from_dict(data_test)
            df_target_test = pd.DataFrame.from_dict(target_test)
            dataTest = id3.data_test(df_data_test, attributes)  # Không có nhãn lớp
            dataTrain_No_Label = id3.data_test(df_data_train, attributes)
            pred = id3.predict(dataTest)
            pred_on_datatrain = id3.predict(dataTrain_No_Label)

            print("------------ĐÁNH GIÁ MÔ HÌNH ID3 --------------------")
            utils = al.utils()
            KFold = al.KFold()
            # Vẽ ma trận nhầm lẫn và tính toán các độ đo
            print("=========Các Độ đo trên tập dữ liệu test=========")
            utils.print_metrics(target_test, pred)
            print("=========Các Độ đo trên tập dữ liệu train=========")
            utils.print_metrics(target_train, pred_on_datatrain)

            # Tính độ đo KFold
            df_full_data = pd.DataFrame.from_dict(full_data)
            df_full_target = pd.DataFrame.from_dict(full_target)
            full_data_target = id3.data_target_merge(df_full_data, df_full_target, attributes,
                                                     "Revenue")  # Tạo tập dữ liệu test có nhãn lớp
            scores = KFold.predict(full_data_target, id3, n_splits=20)
            utils.print_scores(scores)

    elif query == "3.2":
        # Kiểm tra xem GĐ2 đã hoàn thành hay chưa
        isReady = True  # Biến điều khiển có chạy xử lý GĐ2 hay không
        if preProcessState["Xây dựng mô hình theo giải thuật NaiveBayes"] != 1:
            isReady = False
        if isReady:
            print('----------ĐÁNH GIÁ MÔ HÌNH NAIVE BAYES -------')
            print('--------KẾT QUẢ ĐÁNH GIÁ KỸ THUẬT KFOLD 20 LẦN ')
            so = []
            for i in range(20):  # chạy 20 lần
                d, t = al.splitDataset(dataset, 0.7)  # tạo tập test lấy 30% bất kỳ
                so.append(al.getAccuracy(t, al.test(t, dictnv)))  # chạy navi bayes
            utils = al.utils()
            utils.print_scores(so)  # vẽ hình
    elif query == "Exit":
        isContinue = False

# # ======================================GIAI ĐOẠN 1 : TIỀN XỬ LÝ DỮ LIỆU===============================================
#
# # --------------------------------------1.0. PHÂN TÍCH CƠ SỞ DỮ LIỆU --------------------------------------------------
#
# TienXuLy.PhanTichDuLieu(dict_data)
#
# # --------------------------------------1.1. CHỌN LỌC DỮ LIỆU----------------------------------------------------------
#
# # LOẠI BỎ THUỘC TÍNH "OPERATING SYSTEM" VÀ "BROWSER";
# dict_data = TienXuLy.ChonLocDuLieu(dict_data, ["OperatingSystems", "Browser"])
#
# # --------------------------------------1.2. XỬ LÝ SAI & THIẾU DỮ LIỆU--------------------------------------------------
#
# dict_data = TienXuLy.XuLyDuLieuSaiThieu(dict_data)
#
# # --------------------------------------1.3. XỬ LÝ TRÙNG LẶP------------------------------------------------------------
# # LOẠI BỎ CÁC BỘ DỮ LIỆU TRÙNG LẶP VỚI NHAU
# dict_data = TienXuLy.XuLyTrunglap(dict_data)
#
# # --------------------------------------1.4. XỬ LÝ MÂU THUẪN------------------------------------------------------------
#
# # LOẠI BỎ CÁC BỘ DỮ LIỆU MÂU THUẪN VỚI NHAU
# df = pd.DataFrame.from_dict(dict_data)
# dict_data_index = df.to_dict(orient="index");  # Đọc dữ liệu dưới định hướng index. --> Dữ liệu theo dòng.
# dict_data = TienXuLy.XuLyMauThuan(dict_data_index)
#
# # --------------------------------------1.5. XỬ LÝ DỮ LIỆU NHIỄU--------------------------------------------------------
#
# dict_data = TienXuLy.XuLyDuLieuNhieu(dict_data)
#
# # --------------------------------------1.6. PHÂN CHIA TẬP DỮ LIỆU-------------------------------------------------------
#
# arr = TienXuLy.ChiaTapDuLieu(dict_data, 0.3, 0.7)
# # Hàm trả về mảng các tập theo thứ tự sau array[data_train,data_test,target_train,target_test]
# data_train = arr[0]
# data_test = arr[1]
# full_data = np.append(data_train, data_test, axis=0)
# target_train = arr[2]
# target_test = arr[3]
# full_target = np.append(target_train, target_test, axis=0)
# list_attr = list(dict_data.keys())
# del list_attr[-1]
# attributes = list_attr  # Danh sách các thuộc tính điều kiện
#
# # =======================================GIAO ĐOAN 2 : HUẤN LUYỆN TẠO MÔ HÌNH==============================================
#
# # ---------------------------------------2.1.ID3 DECISION TREE ALGORITHM----------------------------------------------
#
# # Tạo dữ liệu đầu vào cho huấn luyện và kiểm thử
# id3 = al.DecisionTree()
# df_data_train = pd.DataFrame.from_dict(data_train)
# df_target_train = pd.DataFrame.from_dict(target_train)
# dataTrain = id3.data_target_merge(df_data_train, df_target_train, attributes, "Revenue")  # Có sẵn nhãn lớp
# id3.fit(dataTrain, attributes)
#
# # ----------------------------------------2.2 NAIVE BAYES ALGORITHM----------------------------------------------------
#
# # df2 = pd.DataFrame.from_dict(dict_data)  # chuyển về dataframe
# # filename = "datasetBZ.csv"
# # df2.to_csv(filename, index=False)  # xuất file csv
# # # df2.to_excel("Result.xlsx")
# # lines = csv.reader(open(filename))#đọc file
# # dataset = list(lines)#chuyển về list
# # del dataset[0]#Xóa nhãn
# # dictnv= al.naivebayes(dataset)#Xây dựng Navi Bayes

#
# # ==========================================GIAI ĐOẠN 3 :  KIỂM THỬ MÔ HÌNH============================================
#
# # -----------------------------------------3.1 ĐÁNH GIÁ MÔ HÌNH ID3--------------------------------------------------
# # NẾU MUỐN KIỂM THỬ BẰNG BỘ DỮ LIỆU NGẪU NHIÊN NHẬP VÀO --> CHUẨN HOÁ BỘ DỮ LIỆU ĐẦU VÀO.
# # CHUẨN HOÁ DỮ LIỆU CẦN PHÂN LỚP
# # data_request=[0,0,0,0,1,0,0.2,0.2,0,0,"Feb",1,1,1,1,"Returning_Visitor",False]
# # data_request=TienXuLy.ChuanHoaDuLieu(dict_data,data_request)
# df_data_test = pd.DataFrame.from_dict(data_test)
# df_target_test = pd.DataFrame.from_dict(target_test)
# dataTest = id3.data_test(df_data_test, attributes)  # Không có nhãn lớp
# dataTrain_No_Label = id3.data_test(df_data_train, attributes)
# pred = id3.predict(dataTest)
# pred_on_datatrain = id3.predict(dataTrain_No_Label)
#
# print("------------ĐÁNH GIÁ MÔ HÌNH ID3 --------------------")
# utils = al.utils()
# KFold = al.KFold()
# # Vẽ ma trận nhầm lẫn và tính toán các độ đo
# print("Các Độ đo trên tập dữ liệu test")
# utils.print_metrics(target_test, pred)
# print("Các Độ đo trên tập dữ liệu train")
# utils.print_metrics(target_train, pred_on_datatrain)
#
# # Tính độ đo KFold
# df_full_data = pd.DataFrame.from_dict(full_data)
# df_full_target = pd.DataFrame.from_dict(full_target)
# full_data_target = id3.data_target_merge(df_full_data, df_full_target, attributes,
#                                          "Revenue")  # Tạo tập dữ liệu test có nhãn lớp
# scores = KFold.predict(full_data_target, id3, n_splits=20)
# utils.print_scores(scores)
#
# # ------------------------------------------3.2. ĐÁNH GIÁ MÔ HÌNH NAIVE BAYES-----------------------------------------------------
# # #Kiem thu 20 lan chế lại của KFold
# # print('----------ĐÁNH GIÁ MÔ HÌNH NAIVE BAYES -------')
# # print('Test KFold 20 lần : ')
# # so = []
# # for i in range(20):# chạy 20 lần
# #     d , t = al.splitDataset(dataset,0.7)# tạo tập test lấy 30% bất kỳ
# #     so.append(al.getAccuracy(t,al.test(t,dictnv)))# chạy navi bayes
# # utils = al.utils()
# # utils.print_scores(so)# vẽ hình